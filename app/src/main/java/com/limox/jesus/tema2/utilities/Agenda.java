package com.limox.jesus.tema2.utilities;

import com.limox.jesus.tema2.model.Contacto;

import java.util.List;

/**
 * Clase agenda que contiene un conjunto Contactos
 */

public class Agenda {
    private List<Contacto> _contactos;

    public List<Contacto> getContacts() {
        return _contactos;
    }

    public void setContacts(List<Contacto> contactos) {
        this._contactos = contactos;
    }

    public Agenda(List<Contacto> contactos) {
        this._contactos = contactos;
    }

    public boolean addContact(Contacto contacto) {
        boolean conExito = true;
        if (!_contactos.contains(contacto)) {
            _contactos.add(contacto);
        } else
            conExito = false;
        return conExito;
    }

    public boolean modifyContact(Contacto oldContacto, Contacto newContacto) {
        boolean conExito = false;

        if (_contactos.contains(oldContacto)) {
            if (!oldContacto.equals(newContacto)) {
                if (!_contactos.contains(newContacto)) {
                    _contactos.set(_contactos.indexOf(oldContacto), newContacto);
                    conExito = true;
                }
            }else{
                _contactos.set(_contactos.indexOf(oldContacto), newContacto);
                conExito = true;
            }
        }
        return conExito;
    }

    public Contacto getContact(Contacto contacto) {
        if (_contactos.contains(contacto))
            return _contactos.get(_contactos.indexOf(contacto));
        else return null;
    }

    public boolean removeContact(Contacto contacto) {
        boolean conExito = false;

        if (_contactos.contains(contacto)) {
            _contactos.remove(contacto);
            conExito = true;
        }
        return conExito;
    }
}
