package com.limox.jesus.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.limox.jesus.tema2.utilities.Ficheros;
import com.limox.jesus.tema2.utilities.Result;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Fertilidad_Activity extends AppCompatActivity {

    TextView txvPeligro;
    TextView txvSinPeligro;
    TextView txvSinConfigurar;
    TextView txvError;
    Button btnConf;
    Ficheros fichero;
    Result result;
    String mensaje;
    Calendar calenSelect;
    Calendar actualDate;
    boolean conDatos;
    int dias;
    int mes;
    int anio;
    int numDias;
    boolean enPeligro = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fertilidad);
        txvPeligro = (TextView) findViewById(R.id.f_txvPeligro);
        txvSinPeligro = (TextView) findViewById(R.id.f_txvSinPeligro);
        txvSinConfigurar = (TextView) findViewById(R.id.f_txvSinDatos);
        txvError = (TextView) findViewById(R.id.f_txvError);
        btnConf = (Button) findViewById(R.id.f_btnConfigurar);
        btnConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Fertilidad_Activity.this, PedirDatosFertilidad_Activity.class), 0);
            }
        });

        fichero = new Ficheros();

        result = fichero.leerExterna("fertil.lx", "UTF-8");

        mensaje = result.getContent();
        try {
            if (!mensaje.isEmpty()) {
                String[] contenido = mensaje.split(";");
                dias = Integer.parseInt(contenido[0]);
                mes = Integer.parseInt(contenido[1]);
                anio = Integer.parseInt(contenido[2]);
                numDias = Integer.parseInt(contenido[3]);
                calenSelect = new GregorianCalendar(anio, mes, dias);
                actualDate = new GregorianCalendar(Locale.getDefault());
                conDatos = true;
                calcularPeligro();
            }

        } catch (Exception e) {
            conDatos = false;
        }

        ponerMensaje();
    }

    private void guardarEnFichero() {
        if (new Ficheros().disponibleEscritura())
            new Ficheros().escribirExterna("fertil.lx", dias + ";" + mes + ";" + anio + ";" + numDias, false, "UTF-8");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 0) {
            Bundle bundle = data.getExtras();
            if (bundle.getBoolean("buenCierre", false)) {
                dias = bundle.getInt("dia", 1);
                mes = bundle.getInt("mes", 1);
                anio = bundle.getInt("anio", 1);
                numDias = bundle.getInt("durCiclo", 1);
                guardarEnFichero();
                calenSelect = new GregorianCalendar(anio, mes, dias);
                actualDate = new GregorianCalendar(Locale.getDefault());
                conDatos = true;
                calcularPeligro();
            }
        }
    }

    void ponerMensaje() {
        if (!fichero.disponibleEscritura()) {
            txvPeligro.setVisibility(View.GONE);
            txvSinPeligro.setVisibility(View.GONE);
            txvSinConfigurar.setVisibility(View.GONE);
            txvError.setVisibility(View.VISIBLE);


        } else if (fichero.existeFicheroExterna("fertil.lx")) {
            if (!conDatos) {
                txvPeligro.setVisibility(View.GONE);
                txvSinPeligro.setVisibility(View.GONE);
                txvSinConfigurar.setVisibility(View.VISIBLE);
                txvError.setVisibility(View.GONE);
            } else {
                if (enPeligro) {
                    txvPeligro.setVisibility(View.VISIBLE);
                    txvSinPeligro.setVisibility(View.GONE);
                    txvSinConfigurar.setVisibility(View.GONE);
                    txvError.setVisibility(View.GONE);
                } else {
                    txvPeligro.setVisibility(View.GONE);
                    txvSinPeligro.setVisibility(View.VISIBLE);
                    txvSinConfigurar.setVisibility(View.GONE);
                    txvError.setVisibility(View.GONE);
                }

            }
        } else {
            txvPeligro.setVisibility(View.GONE);
            txvSinPeligro.setVisibility(View.GONE);
            txvSinConfigurar.setVisibility(View.VISIBLE);
            txvError.setVisibility(View.GONE);
        }
    }

    private boolean calcularPeligro() {
        long result = (calenSelect.getTime()).getTime() - (actualDate.getTime()).getTime();

        //if (isRegular()){
        Calendar fechaPeligro = new GregorianCalendar();
        Calendar[] diasFertiles = new Calendar[3];
        diasFertiles[0] = new GregorianCalendar();
        diasFertiles[1] = new GregorianCalendar();
        diasFertiles[2] = new GregorianCalendar();
        diasFertiles[0].set(anio, mes, dias);
        diasFertiles[1].set(anio, mes, dias);
        diasFertiles[2].set(anio, mes, dias);
        diasFertiles[0].add(Calendar.DAY_OF_MONTH, 16);
        diasFertiles[1].add(Calendar.DAY_OF_MONTH, 17);
        diasFertiles[2].add(Calendar.DAY_OF_MONTH, 18);
        if (actualDate.getTime().getTime() == diasFertiles[0].getTime().getTime() || actualDate.getTime().getTime() == diasFertiles[1].getTime().getTime() || actualDate.getTime().getTime() == diasFertiles[2].getTime().getTime())
            enPeligro = true;
        else
            enPeligro = false;

        ponerMensaje();
        //}

        return true;
    }

    private boolean isRegular() {
        boolean regular = false;
        if (numDias >= 25 && numDias <= 30)
            regular = true;
        return regular;
    }
}
