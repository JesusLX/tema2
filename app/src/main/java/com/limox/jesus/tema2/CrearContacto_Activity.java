package com.limox.jesus.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.limox.jesus.tema2.Singleton.Lists_class;

public class CrearContacto_Activity extends AppCompatActivity {

    EditText edtName;
    EditText edtNumber;
    EditText edtEmail;
    Button btnCrear;
    String number;
    String name;
    String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_contacto);

        edtName = (EditText) findViewById(R.id.crc_edtName);
        edtNumber = (EditText) findViewById(R.id.crc_edtNumber);
        edtEmail = (EditText) findViewById(R.id.crc_edtEmail);
        btnCrear = (Button) findViewById(R.id.crc_btnCrear);

        btnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate()){
                    Lists_class.get().addContacts(Lists_class.ficheroContactos,CrearContacto_Activity.this,name, number, email);
                    startActivity(new Intent(CrearContacto_Activity.this,Agenda_Activity.class));
                    finish();
                }
            }
        });
    }

    private boolean validate(){
        boolean allRigth = true;

        number = edtNumber.getText().toString();
        name = edtName.getText().toString();
        email = edtEmail.getText().toString();

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            allRigth = false;
            edtEmail.setError("Error en el email");
        }
        if (!Patterns.PHONE.matcher(number).matches()){
            allRigth = false;
            edtNumber.setError("Error en el número");
        }
        if (name.isEmpty()){
            allRigth = false;
            edtName.setError("Tienes que introducir un nombre");
        }else
        {
            if (name.contains(";")){
                allRigth = false;
                edtName.setError("No puedes introducir \";\"");
            }
        }
        return allRigth;

    }
}
