package com.limox.jesus.tema2.utilities;

/**
 * Created by jesus on 19/11/16.
 */

public class Result {
    private boolean code; //true es correcto y false indica error
    private String menssage;
    private String content;
    public boolean getCode() {
        return code;
    }
    public void setCode(boolean code) {
        this.code = code;
    }
    public String getMenssage() {
        return menssage;
    }
    public void setMenssage(String menssage) {
        this.menssage = menssage;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }
    public boolean isCode(){
        return code;
    }
}

