package com.limox.jesus.tema2;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.limox.jesus.tema2.utilities.Converter_Class;
import com.limox.jesus.tema2.utilities.Ficheros;
import com.limox.jesus.tema2.utilities.Result;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import cz.msebera.android.httpclient.Header;

public class ConversorMonedas_Activity extends AppCompatActivity implements View.OnClickListener{
    EditText edtDollars, edtEuros, edtServer;
    RadioButton rbtnDollarsToEuros, rbtnEurosToDollars;
    Button btnCalculate, btnDescargar;
    Converter_Class miConversion;
    double valorConversion;
    boolean conValoresParaConvertir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversor_monedas_);
        edtDollars = (EditText) findViewById(R.id.cm_edtDollars);
        edtEuros = (EditText) findViewById(R.id.cm_edtEuros);
        edtServer = (EditText) findViewById(R.id.cm_edtServer);
        rbtnDollarsToEuros = (RadioButton) findViewById(R.id.cm_rbtnDollars);
        rbtnEurosToDollars = (RadioButton) findViewById(R.id.cm_rbtnEuros);
        btnCalculate = (Button) findViewById(R.id.cm_btnConvert);
        btnCalculate.setOnClickListener(this);
        btnDescargar = (Button) findViewById(R.id.cm_btnDescargar);
        btnDescargar.setOnClickListener(this);

        miConversion = new Converter_Class();

        conValoresParaConvertir = false;

        btnDescargar.callOnClick();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cm_btnConvert:
                String valorCoin;
                try {

                    // if the var changeValor is not empty, will make the conversion
                    if (conValoresParaConvertir) {
                        // Check which of the radioButtons are checked to make the correct conversion
                        if (rbtnDollarsToEuros.isChecked()) {
                            // convert dollars to euros
                            valorCoin = edtDollars.getText().toString();
                            if (valorCoin.isEmpty() || valorCoin == ".")
                                valorCoin = "0";

                            edtEuros.setText(String.valueOf(miConversion.dollarsToEuros(Double.parseDouble(valorCoin), valorConversion)));
                        } else {

                            valorCoin = edtEuros.getText().toString();
                            if (valorCoin.isEmpty() || valorCoin == ".")
                                valorCoin = "0";
                            edtDollars.setText(String.valueOf(miConversion.eurosToDollars(Double.parseDouble(valorCoin), valorConversion)));
                        }
                    }else
                        Toast.makeText(ConversorMonedas_Activity.this, "No hay valores de conversion establecidos", Toast.LENGTH_LONG).show();

                } catch (NumberFormatException ex) {
                    Toast.makeText(this, "Error al convertir el numero", Toast.LENGTH_LONG).show();
                } catch (NullPointerException ex) {
                    Toast.makeText(this, "Error al convertir el numero", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.cm_btnDescargar: //Por defecto coge el servidor de joselu
                if (isNetworkAvailable()){
                    String ruta = edtServer.getText().toString();
                    if (ruta.isEmpty()){
                        Toast.makeText(this, "Debes introducir una ruta al fichero", Toast.LENGTH_LONG).show();
                    }else
                        descargarFichero(ruta);
                }else
                    Toast.makeText(this, "No hay conexion a internet", Toast.LENGTH_LONG).show();

                break;

        }

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    private void descargarFichero(String ruta){
        AsyncHttpClient client = new AsyncHttpClient();
        final ProgressDialog progress = new ProgressDialog(ConversorMonedas_Activity.this);
//      Get the the server edtServer
        client.get(ruta, new FileAsyncHttpResponseHandler(/* Context */ this) {
            @Override
            public void onStart() {
            // Start
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
            // Failure
                //En caso de error se detiene la barra de progreso y se informa al usuario
                progress.dismiss();
                Toast.makeText(ConversorMonedas_Activity.this, "Error " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {
                // Do something with the file `response`
                progress.dismiss();
                Result rst = new Ficheros().leer(response,"UTF-8");

                String valorC = rst.getContent();

                try {
                    valorConversion = Double.parseDouble(valorC);
                    Toast.makeText(ConversorMonedas_Activity.this, "Valor de conversion establecido a "+valorConversion, Toast.LENGTH_LONG).show();
                    conValoresParaConvertir = true;
                }catch (Exception e){
                    Toast.makeText(ConversorMonedas_Activity.this, "Error con la lectura del archivo", Toast.LENGTH_LONG).show();
                    conValoresParaConvertir = false;
                }

            }
        });
    }
}
