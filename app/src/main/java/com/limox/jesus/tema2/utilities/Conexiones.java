package com.limox.jesus.tema2.utilities;

import android.os.Environment;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

/**
 * Created by jesus on 19/11/16.
 */

public class Conexiones {
    public static String conectarJava(String lautl) {
        URL url;
        HttpURLConnection urlConnection = null;
        int respuesta;
        String body = " ";

        try {
            url = new URL(lautl);
            urlConnection = (HttpURLConnection) url.openConnection();
            respuesta = urlConnection.getResponseCode();
            if (respuesta == HttpURLConnection.HTTP_OK)
                body = leer(urlConnection.getInputStream());
            else
                body = "Error en el acceso a la web: "
                        + String.valueOf(respuesta);
        } catch (MalformedURLException e) {
            body = "Excepcion por URL incorrecta: " + e.getMessage();
        } catch (SocketTimeoutException e) {
            body = "Excepcion por timeout: " + e.getMessage();
        } catch (Exception e) {
            body = "Excepcion: " + e.getMessage();
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
            } catch (Exception e) {
                body = "Excepcion: " + e.getMessage();
            }

        }
        return body;
    }

    public static String conectarApache(String texto) {
        HttpClient cliente;
        /*HttpPost*/
        HttpGet peticion;
        HttpResponse respuesta;
        String body = " ";
        int codigo;
        try {
            cliente = new DefaultHttpClient();
            peticion = new HttpGet(texto);
            respuesta = cliente.execute(peticion);
            codigo = respuesta.getStatusLine().getStatusCode();
            if (codigo == HttpURLConnection.HTTP_OK)
                body = leer(respuesta.getEntity().getContent());
            else
                body = "Error en el acceso a la web: " + String.valueOf(codigo);
        } catch (Exception e) {
            body = "Excepcion: " + e.getMessage();
        }
        return body;
    }
    /*public static Result conectarApache(String texto) {
        CloseableHttpClient cliente = null;
        HttpPost peticion;
        HttpResponse respuesta;
        int valor;
        Result resultado = new Result();
        try {
            //cliente = new DefaultHttpClient();
            cliente = HttpClientBuilder.create().build();
            peticion = new HttpPost(texto);
            respuesta = cliente.execute(peticion);
            valor = respuesta.getStatusLine().getStatusCode();
            if (valor == HttpURLConnection.HTTP_OK) {
                resultado.setCode(true);
                resultado.setContent(leer(respuesta.getEntity().getContent()));
            }
            else {
                resultado.setCode(false);
                resultado.setMenssage("Error en el acceso a la web: " + String.valueOf(valor));
            }
            cliente.close();
        } catch (IOException e) {
            resultado.setCode(false);
            resultado.setMenssage("Excepción: " + e.getMessage());
            if (cliente != null)
                try {
                    cliente.close();
                } catch (IOException excep) {
                    resultado.setCode(false);
                    resultado.setMenssage("Excepción: " + excep.getMessage());
                }
        }
        return resultado;
    }*/
    private static String leer(InputStream inputStream) {
        StringBuffer pagina = new StringBuffer();

        String line = "";
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(
                    inputStream));
            while ((line = rd.readLine()) != null)
                pagina.append(line);

            rd.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pagina.toString();
    }

}
