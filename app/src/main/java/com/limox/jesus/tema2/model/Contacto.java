package com.limox.jesus.tema2.model;

/**
 * Created by jesus on 30/10/16.
 */

public class Contacto {
    String _numero;
    String _nombre;
    String _email;
    String _descripcion;

    //region GetersAndSetters
    public String getNumero() {
        return _numero;
    }

    public void setNumero(String _numeron) {
        this._numero = _numeron;
    }

    public String getNombre() {
        return _nombre;
    }

    public void setNombre(String _nombre) {
        this._nombre = _nombre;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public String getDescripcion() {
        return _descripcion;
    }

    public void setDescripcion(String _descripcion) {
        this._descripcion = _descripcion;
    }
    //endregion

    //region Constructores

    public Contacto(String _numeron) {
        this._numero = _numeron;
        this._nombre = new String();
        this._email  = new String();
        this._descripcion  = new String();
    }

    public Contacto(String _nombre, String _numeron) {
        this._nombre = _nombre;
        this._numero = _numeron;
        this._email  = new String();
        this._descripcion  = new String();
    }

    public Contacto( String _nombre, String _numero , String _email) {
        this._nombre = _nombre;
        this._numero = _numero;
        this._email  = _email;
    }

    public Contacto(String _numeron, String _nombre, String _email, String _descripcion) {
        this._numero = _numeron;
        this._nombre = _nombre;
        this._email = _email;
        this._descripcion = _descripcion;
    }
    //endregion


    @Override
    public String toString() {
        return "Número: " + _numero +
                ", Nombre: " + _nombre +
                ", Email: " + _email
                 ;
    }

    @Override
    public boolean equals(Object o) {
        boolean result;
        result = (this == o);
        result = (o == null || getClass() != o.getClass());// return false;
        if (!result) {
            Contacto contacto = (Contacto) o;

            result = (_numero != null ? !_numero.equals(contacto._numero) : contacto._numero != null);
            result =  _nombre != null ? _nombre.equals(contacto._nombre) : contacto._nombre == null;
        }
        return result;
    }

    @Override
    public int hashCode() {
        int result = _numero != null ? _numero.hashCode() : 0;
        result = 31 * result + (_nombre != null ? _nombre.hashCode() : 0);
        return result;
    }
}
