package com.limox.jesus.tema2;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.limox.jesus.tema2.Singleton.Alarmas_Singleton;

import java.util.ArrayList;

public class Alarmas_Activity extends AppCompatActivity {

    MyCountdown countdown;
    TextView txvAlarma1;
    TextView txvAlarma2;
    TextView txvAlarma3;
    TextView txvAlarma4;
    TextView txvAlarma5;
    Button btnStart;
    Button btnStop;
    int[] minutos;
    ArrayList<String> textos;
    ArrayList<TextView> textvws;
    int currentCounter;
    int defaultColor;

    MediaPlayer mp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarmas);

        btnStart = (Button) findViewById(R.id.al_btnComenzar);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCountdown(0);
            }
        });
        btnStop = (Button) findViewById(R.id.al_btnParar);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopCountdown();
            }
        });

        txvAlarma1 = (TextView) findViewById(R.id.al_txvAlarma1);
        txvAlarma2 = (TextView) findViewById(R.id.al_txvAlarma2);
        txvAlarma3 = (TextView) findViewById(R.id.al_txvAlarma3);
        txvAlarma4 = (TextView) findViewById(R.id.al_txvAlarma4);
        txvAlarma5 = (TextView) findViewById(R.id.al_txvAlarma5);
        defaultColor = txvAlarma1.getCurrentTextColor();
        minutos = Alarmas_Singleton.getInstance().getAllMinutes();
        textos = new ArrayList<>(Alarmas_Singleton.getInstance().getAllText());

        textvws = new ArrayList<>();
        textvws.add(txvAlarma1);
        textvws.add(txvAlarma2);
        textvws.add(txvAlarma3);
        textvws.add(txvAlarma4);
        textvws.add(txvAlarma5);

        setTexts();

        currentCounter = minutosASegundos(minutos[0]);
        mp = MediaPlayer.create(this, R.raw.alarma);
    }

    private void setTexts() {
        for (int i = 0; i < minutos.length; i++) {
            switch (i) {
                case 0:
                    txvAlarma1.setText(segundosAFormatoMunutos(minutosASegundos(minutos[i])) + " " + textos.get(i));
                    txvAlarma1.setTextColor(defaultColor);
                    break;
                case 1:
                    txvAlarma2.setText(segundosAFormatoMunutos(minutosASegundos(minutos[i])) + " " + textos.get(i));
                    txvAlarma2.setTextColor(defaultColor);
                    break;
                case 2:
                    txvAlarma3.setText(segundosAFormatoMunutos(minutosASegundos(minutos[i])) + " " + textos.get(i));
                    txvAlarma3.setTextColor(defaultColor);
                    break;
                case 3:
                    txvAlarma4.setText(segundosAFormatoMunutos(minutosASegundos(minutos[i])) + " " + textos.get(i));
                    txvAlarma4.setTextColor(defaultColor);
                    break;
                case 4:
                    txvAlarma5.setText(segundosAFormatoMunutos(minutosASegundos(minutos[i])) + " " + textos.get(i));
                    txvAlarma5.setTextColor(defaultColor);
                    break;

            }
        }
    }

    private int minutosASegundos(int minutos) {
        int segundos;
        segundos = minutos * 60;
        return segundos;
    }

    long segundosAMilisegundos(int segundos) {
        return segundos * 1000;
    }

    private String segundosAFormatoMunutos(int segundos) {
        String minutos;
        minutos = String.format("%d:%02d", (segundos / 60), (segundos % 60));
        return minutos;
    }

    private void startCountdown(int i) {
        btnStart.setEnabled(false);
        btnStop.setEnabled(true);
        countdown = new MyCountdown(segundosAMilisegundos(minutosASegundos(minutos[i])), 1000, textvws.get(i), minutos[i], textos.get(i), textvws.size(), i);
        countdown.start();
    }

    private void stopCountdown() {
        btnStart.setEnabled(true);
        btnStop.setEnabled(false);
        setTexts();
        countdown.cancel();
    }

    protected class MyCountdown extends CountDownTimer {

        TextView textView;
        int segundos;
        String menssage;
        int numFilas;
        int currentPosition;
        int color;

        public MyCountdown(long millisInFuture, long countDownInterval, TextView dondeve, int minutos, String mensaje, int nFilas, int currentFila) {
            super(millisInFuture, countDownInterval);
            segundos = minutosASegundos(minutos) - 1;
            menssage = mensaje;
            textView = dondeve;
            numFilas = nFilas;
            color = textView.getCurrentTextColor();
            textView.setTextColor(getResources().getColor(R.color.colorAccent));
            currentPosition = currentFila;

        }

        @Override
        public void onTick(long millisUntilFinished) {
            segundos--;
            textView.setText(segundosAFormatoMunutos(segundos) + " " + menssage);
        }

        @Override
        public void onFinish() {
            textView.setTextColor(color);
            mp.start();
            Toast.makeText(Alarmas_Activity.this, "Alarma "+currentPosition+" ha terminado", Toast.LENGTH_SHORT).show();
            if (currentPosition + 1 < numFilas) {
                startCountdown(currentPosition + 1);
            }

        }
    }

}
