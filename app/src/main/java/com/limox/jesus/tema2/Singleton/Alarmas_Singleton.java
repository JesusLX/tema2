package com.limox.jesus.tema2.Singleton;

import android.os.Environment;

import com.limox.jesus.tema2.model.Alarma;
import com.limox.jesus.tema2.utilities.Ficheros;
import com.limox.jesus.tema2.utilities.Result;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesus on 17/11/16.
 */
public class Alarmas_Singleton {
    private static Alarmas_Singleton ourInstance = new Alarmas_Singleton();
    ArrayList<Alarma> mAlarmas;
    Ficheros ficheros = new Ficheros();

    public static Alarmas_Singleton getInstance() {
        return ourInstance;
    }

    private Alarmas_Singleton() {
        mAlarmas = new ArrayList<>();
        StringBuilder alarmas = new StringBuilder();
        Result result;
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), "alarmas.lx");
        if (!miFichero.exists()) {
            mAlarmas.add(new Alarma(5, "Primera alarma"));
            mAlarmas.add(new Alarma(4, "Segunda alarma"));
            mAlarmas.add(new Alarma(3, "Tercera alarma"));
            mAlarmas.add(new Alarma(2, "Cuarta alarma"));
            mAlarmas.add(new Alarma(1, "Quinta alarma"));
            for (int i = 0; i < mAlarmas.size(); i++) {
                alarmas.append(mAlarmas.get(i).toString() + "\n");
            }
            ficheros.escribirExterna("alarmas.lx", alarmas.toString(), false, "UTF-8");
        }else {
            result = ficheros.leerExterna("alarmas.lx", "UTF-8");
            String contenido = result.getContent();
            String[] lineas = contenido.split("\n");
            for (int i = 0; i < lineas.length; i++) {
                String[] alarm = lineas[i].split(";");
                mAlarmas.add(new Alarma(Integer.parseInt(alarm[0]),alarm[1]));
            }
        }
    }

    public List<Alarma> getAlarmas(){
        return mAlarmas;
    }

    public Alarma getAlarmaByPosition(int position){
        if (position > mAlarmas.size()-1){
            position = mAlarmas.size()-1;
        }else
        if (position < 0){
            position = 0;
        }

        return mAlarmas.get(position);
    }

    public int[] getAllMinutes(){
        int[] minutos = new int[mAlarmas.size()];
        for (int i = 0; i < mAlarmas.size(); i++) {
            minutos[i] = mAlarmas.get(i).getMinutos();
        }
        return minutos;
    }
    public List<String> getAllText(){
        ArrayList<String> textos = new ArrayList<>();
        for (int i = 0; i < mAlarmas.size(); i++) {
            textos.add(mAlarmas.get(i).getMensaje());
        }
        return textos;
    }


}
