package com.limox.jesus.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Index_Activity extends AppCompatActivity implements View.OnClickListener {

    //region Recursos
    Button btnEjercicio1, btnEjercicio2, btnEjercicio3,
            btnEjercicio4, btnEjercicio5, btnEjercicio6,
            btnEjercicio7;
    //endregion


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);
        //region DeclaracionBotones
        btnEjercicio1 = (Button) findViewById(R.id.btn1);
        btnEjercicio2 = (Button) findViewById(R.id.btn2);
        btnEjercicio3 = (Button) findViewById(R.id.btn3);
        btnEjercicio4 = (Button) findViewById(R.id.btn4);
        btnEjercicio5 = (Button) findViewById(R.id.btn5);
        btnEjercicio6 = (Button) findViewById(R.id.btn6);
        btnEjercicio7 = (Button) findViewById(R.id.btn7);
        //endregion
        //region SetOnClickListeners
        btnEjercicio1.setOnClickListener(this);
        btnEjercicio2.setOnClickListener(this);
        btnEjercicio3.setOnClickListener(this);
        btnEjercicio4.setOnClickListener(this);
        btnEjercicio5.setOnClickListener(this);
        btnEjercicio6.setOnClickListener(this);
        btnEjercicio7.setOnClickListener(this);
        //endregion
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn1:
                intent = new Intent(Index_Activity.this, Agenda_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn2:
                intent = new Intent(Index_Activity.this, Alarmas_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn3:
                intent = new Intent(Index_Activity.this, Fertilidad_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn4:
                intent = new Intent(Index_Activity.this, ExploradorWeb_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn5:
                intent = new Intent(Index_Activity.this, ExploradorImagenes_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn6:
                intent = new Intent(Index_Activity.this, ConversorMonedas_Activity.class);
                startActivity(intent);
                break;
            case R.id.btn7:
                intent = new Intent(Index_Activity.this, SubirFicheros_Activity.class);
                startActivity(intent);
                break;
        }
    }
}
