package com.limox.jesus.tema2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.limox.jesus.tema2.Adapter.ContactsAdapter;


/**
 * Created by jesus on 30/10/16.
 */
public class Agenda_Activity extends AppCompatActivity {
   Button btnAdd;
    ListView lvContactos;
    ContactsAdapter adapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);

        btnAdd = (Button) findViewById(R.id.ag_btnAdd);
        lvContactos = (ListView) findViewById(R.id.ag_lvListContactos);

        adapter = new ContactsAdapter(this);
        lvContactos.setAdapter(adapter);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(Agenda_Activity.this,CrearContacto_Activity.class));
                finish();
            }
        });
        adapter.notifyDataSetChanged();



    }

}
