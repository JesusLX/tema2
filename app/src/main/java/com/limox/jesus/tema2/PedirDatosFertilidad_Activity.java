package com.limox.jesus.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.NumberPicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class PedirDatosFertilidad_Activity extends AppCompatActivity {

    Button btnOk;
    NumberPicker np;
    boolean buenCierre;
    DatePicker datePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedir_datos_fertilidad);

        datePicker = (DatePicker) findViewById(R.id.pdf_dPUltimaRegla);
        Calendar actualDate = new GregorianCalendar(Locale.getDefault());
        datePicker.setMaxDate(actualDate.getTimeInMillis());

        buenCierre = false;

        btnOk = (Button) findViewById(R.id.pdf_btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buenCierre = true;
                finish();
            }
        });

        np = (NumberPicker) findViewById(R.id.pdf_nPDuracionCiclo);
        String[] nums = new String[15];
        for (int i = 0; i < nums.length; i++)
            nums[i] = Integer.toString(i + 21);

        np.setMinValue(21);
        np.setMaxValue(35);
        np.setWrapSelectorWheel(false);
        np.setDisplayedValues(nums);
        np.setValue(21);

    }

    @Override
    public void finish() {
        Intent i = getIntent();
        Bundle bundle = new Bundle();
        if (buenCierre) {
            bundle.putInt("dia", datePicker.getDayOfMonth());
            bundle.putInt("mes", datePicker.getMonth());
            bundle.putInt("anio", datePicker.getYear());
            bundle.putInt("durCiclo", np.getValue());
        }
        bundle.putBoolean("buenCierre", buenCierre);
        i.putExtras(bundle);
        setResult(0, i);
        super.finish();
    }
}
