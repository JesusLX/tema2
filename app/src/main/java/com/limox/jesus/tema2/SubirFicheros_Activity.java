package com.limox.jesus.tema2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.limox.jesus.tema2.R;
import com.limox.jesus.tema2.utilities.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

public class SubirFicheros_Activity extends AppCompatActivity {

    EditText edtServidor;
    TextView txvArchivo;
    Button btnBuscarArchivo, btnSubirArchivo;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subir_ficheros);
        edtServidor = (EditText) findViewById(R.id.sf_edtUrlServidor);
        txvArchivo = (TextView) findViewById(R.id.sf_txvRuta);
        btnBuscarArchivo = (Button) findViewById(R.id.sf_btnBuscar);
        btnSubirArchivo = (Button) findViewById(R.id.sf_btnSubir);
        btnBuscarArchivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Lanzamos el explorador comprobando que existe alguno
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("file/*");
                if (i.resolveActivity(getPackageManager()) != null) {

                    startActivityForResult(i, 1);

                } else {

                    Toast.makeText(SubirFicheros_Activity.this, "No hay un explorador de archivos instalado", Toast.LENGTH_LONG).show();
                }
            }
        });
        btnSubirArchivo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (file != null) {
                        subida();
                    } else {
                        Toast.makeText(SubirFicheros_Activity.this, "No hay un archivo seleccionado", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(SubirFicheros_Activity.this, "No hay una conexion a la red", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    private boolean isNetworkAvailable() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            file = new File(data.getData().getPath());
            txvArchivo.setText(file.getAbsolutePath());
        }


    }

    private void subida() {
        String fichero = txvArchivo.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(SubirFicheros_Activity.this);
        File myFile;
        Boolean existe = true;
        myFile = new File(Environment.getExternalStorageDirectory(), fichero);

        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
        } catch (FileNotFoundException e) {
            existe = false;
            Toast.makeText(this, "Error en el fichero: " + e.getMessage(), Toast.LENGTH_LONG);
        }
        String servidor = edtServidor.getText().toString();

        if (!servidor.isEmpty()) {
            if (existe)
                RestClient.post(servidor, params, new TextHttpResponseHandler() {
                    @Override
                    public void onStart() {
                        // called before request is started
                        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progreso.setMessage("Conectando . . .");
                        //progreso.setCancelable(false);
                        progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                RestClient.cancelRequests(getApplicationContext(), true);
                            }
                        });
                        progreso.show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        // called when response HTTP status is "200 OK"
                        progreso.dismiss();
                        Toast.makeText(SubirFicheros_Activity.this, "Fichero subido", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        progreso.dismiss();
                        Toast.makeText(SubirFicheros_Activity.this, "Error al subir el fichero", Toast.LENGTH_LONG).show();

                    }
                });
        }
        Toast.makeText(SubirFicheros_Activity.this, "Error al conectar con el servidor", Toast.LENGTH_LONG).show();
    }
}
