package com.limox.jesus.tema2.utilities;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesus on 6/11/16.
 */

public class Ficheros {

    public Ficheros() {

    }

    public boolean existeFicheroExterna(String fichero){

        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);
        return miFichero.exists();

    }

    private boolean escribir(File fichero, String cadena, Boolean anadir, String codigo) {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter out = null;
        boolean correcto = false;
        try {
            fos = new FileOutputStream(fichero, anadir);
            osw = new OutputStreamWriter(fos, codigo);
            out = new BufferedWriter(osw);
            out.write(cadena);
        } catch (IOException e) {
            Log.e("Error de E/S", e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                    correcto = true;
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return correcto;
    }

    public List<String> leerListaInterna(String fichero, Context context) {
        ArrayList<String> contenido = new ArrayList<String>();
        File file = new File(context.getFilesDir(),fichero);

        try
        {
            if (!file.exists()){
                file.createNewFile();
            }
            BufferedReader fin =new BufferedReader(new InputStreamReader(context.openFileInput(fichero)));
            String linea;
            while ((linea = fin.readLine())!= null) {
                contenido.add(linea);
            }
            fin.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contenido;
    }

    public String leerInterna(String fichero, Context context) {
        StringBuilder contenido = new StringBuilder();
        try {
            FileReader fr = new FileReader(fichero);
            BufferedReader br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null)
                contenido.append(linea);

            fr.close();
        } catch (Exception e) {
            System.out.println("Excepcion leyendo fichero " + fichero + ": " + e);
        }
        return contenido.toString();
    }

    public boolean escribirInterna(Context context, String fichero, List<String> contenido) {
        boolean correcto = true;

        try {
            PrintWriter _ficheroW;
           File tmpfile = new File(context.getFilesDir(),fichero);
            if (!tmpfile.exists()){
                tmpfile.createNewFile();
            }
            _ficheroW = new PrintWriter(tmpfile);

            for (String s : contenido) {
                _ficheroW.println(s);
            }
            _ficheroW.close();
        } catch (IOException e) {
            e.printStackTrace();
            correcto = false;
        }
        return correcto;
    }

    public boolean disponibleEscritura(){
        boolean escritura = false;
        //Comprobamos el estado de la memoria externa (tarjeta SD)
        String estado = Environment.getExternalStorageState();
        if (estado.equals(Environment.MEDIA_MOUNTED))
            escritura = true;
        return escritura;
    }

    public boolean escribirExterna(String fichero, String cadena, Boolean anadir, String
            codigo) {
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);
        return escribir(miFichero, cadena, anadir, codigo);
    }
    public Result leerExterna(String fichero, String codigo){
        File miFichero, tarjeta;
        tarjeta = Environment.getExternalStorageDirectory();
        miFichero = new File(tarjeta.getAbsolutePath(), fichero);
        return leer(miFichero, codigo);
    }
    public Result leer(File fichero, String codigo){
        FileInputStream fis = null;
        InputStreamReader isw = null;
        BufferedReader in = null;
        //String linea;
        StringBuilder miCadena = new StringBuilder();
        Result result = new Result();
        int n;
        result.setCode(true);
        try {
            fis = new FileInputStream(fichero);
            isw = new InputStreamReader(fis, codigo);
            in = new BufferedReader(isw);
            while ((n = in.read()) != -1)
                miCadena.append((char) n);
            //while ((linea = in.readLine()) != null)
            //miCadena.append(linea).append('\n');
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
            result.setCode(false);
            result.setMenssage(e.getMessage());
        } finally {
            try {
                if (in != null) {
                    in.close();
                    result.setContent(miCadena.toString());
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
                result.setCode(false);
                result.setMenssage(e.getMessage());
            }
        }
        return result;
    }
}
