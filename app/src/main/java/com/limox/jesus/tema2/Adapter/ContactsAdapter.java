package com.limox.jesus.tema2.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.limox.jesus.tema2.R;
import com.limox.jesus.tema2.model.Contacto;
import com.limox.jesus.tema2.Singleton.Lists_class;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesus on 6/11/16.
 */

public class ContactsAdapter extends ArrayAdapter<Contacto> {

    private Context context;
    List<Contacto> contactos;


    public ContactsAdapter(Context context) {
        super(context, R.layout.card_contacts,Lists_class.get().getContactos(Lists_class.ficheroContactos,context));
        this.context = context;
        contactos = new ArrayList<Contacto>(Lists_class.get().getContactos(Lists_class.ficheroContactos,context)) ;

    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // The Best way

        View item = convertView;
        ProductHolder productHolder;

        // If the convertView isn't in memory create it, else use the created
        if (item == null) {

            //1. Create a object inflater who initializates to LayourInflater of the proyecy
            //LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            // or
            LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            // 2.
            // Inflate view
            // Inflate the itemProduct and the parent go null to put a parent
            // This create at memory the objects View who contains the xml
            item = layoutInflater.inflate(R.layout.card_contacts, null);
            productHolder = new ProductHolder();
            //3
            // Asignate to the vars the widgets using findViewById
            // Make it simple cause is already charged in memory for inflate the view
            productHolder.txvName_item = (TextView) item.findViewById(R.id.cc_txvNameContact);
            productHolder.txvNumber_item = (TextView) item.findViewById(R.id.cc_txvPhone);
            productHolder.txvEmail_item = (TextView) item.findViewById(R.id.cc_txvEmail);

            // !!!!!!!!!!!!!!!!!!!!!!
            item.setTag(productHolder);
        } else                                // !!!!For this!!!!
            productHolder = (ProductHolder) item.getTag();


        //4
        // Asignate the datas of the adapter of the widgets
        productHolder.txvName_item.setText(getItem(position).getNombre());
        productHolder.txvNumber_item.setText(getItem(position).getNumero());
        productHolder.txvEmail_item.setText(getItem(position).getEmail());

        return item;
    }

    /**
     * Internal class
     */
    class ProductHolder {
        TextView txvName_item;
        TextView txvNumber_item;
        TextView txvEmail_item;
    }
}
