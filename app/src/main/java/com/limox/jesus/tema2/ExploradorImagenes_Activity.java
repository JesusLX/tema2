package com.limox.jesus.tema2;

import android.app.ProgressDialog;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.limox.jesus.tema2.R;
import com.limox.jesus.tema2.utilities.Ficheros;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;

public class ExploradorImagenes_Activity extends AppCompatActivity {

    Button btnDescargar, btnSiguiente, btnAnterior;
    EditText edtUrlFichero;
    ImageView iVImagenes;
    String[] imagenes;
    int posArray;
    int imagenError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorador_imagenes);
        posArray = 0;
        btnDescargar = (Button) findViewById(R.id.ei_btnDescargar);
        btnSiguiente = (Button) findViewById(R.id.ei_btnSiguiente);
        btnAnterior = (Button) findViewById(R.id.ei_btnAtras);

        edtUrlFichero = (EditText) findViewById(R.id.ei_edtFichero);

        iVImagenes = (ImageView) findViewById(R.id.ei_iVImagen);
        imagenError = R.drawable.error;


        btnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (!edtUrlFichero.getText().toString().isEmpty()) {
                        conectarAsincrono(edtUrlFichero.getText().toString());
                    } else {
                        Toast.makeText(ExploradorImagenes_Activity.this, "Introduce la ruta hacia el fichero", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(ExploradorImagenes_Activity.this, "No hay conexion a internet", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargarImagenSiguiente();
            }
        });
        btnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cargarImagenAnterior();
            }
        });

    }

    private void cargarImagen() {
        if (imagenes.length > 0) {
            Picasso.with(ExploradorImagenes_Activity.this).load(imagenes[0]).error(imagenError).into(iVImagenes);
        }
    }

    private void cargarImagenSiguiente() {
        if (imagenes != null)
            if (imagenes.length > 0) {
                posArray++;
                if (posArray > imagenes.length-1)
                    posArray = 0;
                try{
                    Picasso.with(ExploradorImagenes_Activity.this).load(imagenes[posArray]).error(imagenError).into(iVImagenes);
                }catch (Exception e){
                    Toast.makeText(this,"Error al cargar la imagen"+posArray+" "+e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
    }

    private void cargarImagenAnterior() {
        if (imagenes != null)
            if (imagenes.length > 0) {
                posArray--;
                if (posArray < 0)
                    posArray = imagenes.length - 1;
                try{
                    Picasso.with(ExploradorImagenes_Activity.this).load(imagenes[posArray]).error(imagenError).into(iVImagenes);
                }catch (Exception e){
                    Toast.makeText(this,"Error al cargar la imagen"+posArray+" "+e.getMessage(),Toast.LENGTH_LONG).show();
                }
            }
    }

    private boolean isNetworkAvailable() {
        boolean result = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            result = true;

        return result;

    }

    private void conectarAsincrono(String url) {

        AsyncHttpClient client = new AsyncHttpClient();
        final ProgressDialog progress = new ProgressDialog(ExploradorImagenes_Activity.this);
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                //En caso de error se detiene la barra de progreso y se informa al usuario
                progress.dismiss();
                Toast.makeText(ExploradorImagenes_Activity.this, "Error " + throwable.getMessage(), Toast.LENGTH_LONG).show();


            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                progress.dismiss();
                imagenes = responseString.split("\n");

                Toast.makeText(ExploradorImagenes_Activity.this, "Se han guardado " + imagenes.length + " imagenes", Toast.LENGTH_LONG).show();

                cargarImagen();
            }

            @Override
            public void onStart() {
                // called before request is started
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }
        });
    }
}
