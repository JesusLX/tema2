package com.limox.jesus.tema2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.limox.jesus.tema2.utilities.Ficheros;
import com.limox.jesus.tema2.utilities.Result;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.Header;

public class ExploradorWeb_Activity extends AppCompatActivity {

    WebView wVExplorador;
    TextView txvDuracion;
    RadioGroup rgTiposConexion;
    RadioButton rBJava;
    RadioButton rBAahc;
    RadioButton rBVolley;
    EditText edtUrl;
    EditText edtFichero;
    Button btnBuscar;
    Button btnGuardar;
    String url;
    String pagina = " ";
    StringBuilder contenidoWeb;
    long inicio, fin;
    public final static String JAVA = "java";
    public final static String AAHC = "aahc";
    public final static String VOLLEY = "volley";
    String conexionSeleccionada = JAVA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorador_web);
        wVExplorador = (WebView) findViewById(R.id.ew_wVExplorador);
        txvDuracion = (TextView) findViewById(R.id.ew_txvTiempo);
        rgTiposConexion = (RadioGroup) findViewById(R.id.ew_rGTiposConexion);
        rBJava = (RadioButton) findViewById(R.id.ew_rBtnJava);
        rBAahc = (RadioButton) findViewById(R.id.ew_rBtnAahc);
        rBVolley = (RadioButton) findViewById(R.id.ew_rBtnVolley);
        btnBuscar = (Button) findViewById(R.id.ew_btnBuscar);
        btnGuardar = (Button) findViewById(R.id.ew_btnGuardar);
        edtUrl = (EditText) findViewById(R.id.ew_edtUrl);
        edtFichero = (EditText) findViewById(R.id.ew_edtFichero);

        contenidoWeb = new StringBuilder();

        rgTiposConexion.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                switch (i) {

                    case R.id.ew_rBtnJava:
                        conexionSeleccionada = JAVA;
                        break;
                    case R.id.ew_rBtnAahc:
                        conexionSeleccionada = AAHC;
                        break;
                    case R.id.ew_rBtnVolley:
                        conexionSeleccionada = VOLLEY;
                        break;

                }
            }
        });
        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isNetworkAvailable()) {
                    if (edtUrl.getText().toString().isEmpty())
                        Toast.makeText(ExploradorWeb_Activity.this, "No hay una url establecida", Toast.LENGTH_LONG).show();
                    else {
                        url = edtUrl.getText().toString();
                        conectar(url);
                    }
                } else {
                    Toast.makeText(ExploradorWeb_Activity.this, "Error en la conexión de internet", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new Ficheros().disponibleEscritura()){
                    if (edtFichero.getText().toString().isEmpty())
                        Toast.makeText(ExploradorWeb_Activity.this, "Escribe un nombre para el fichero", Toast.LENGTH_LONG).show();
                    else {
                        conexionSeleccionada = AAHC;
                        btnBuscar.callOnClick();
                        new Ficheros().escribirExterna(edtFichero.getText().toString(),contenidoWeb.toString(),false,"UTF-8");
                        Toast.makeText(ExploradorWeb_Activity.this, "Fichero guardado", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private void conectar(String miUrl) {
        HttpURLConnection urlConnection = null;
        switch (conexionSeleccionada) {
            case JAVA:
                ConectionAsynt conext = new ConectionAsynt(ExploradorWeb_Activity.this, "UTF-8");
                conext.execute(edtUrl.getText().toString());
                break;
            case AAHC:
                conectarAAHC(url);
                break;
            case VOLLEY:
                conectarVolley(url);
                break;

        }
    }
    private void Guardar(String nombreFichero){

    }
    private void conectarVolley(String miUrl){

        RequestQueue queue = Volley.newRequestQueue(ExploradorWeb_Activity.this);
        final ProgressDialog progress = new ProgressDialog(ExploradorWeb_Activity.this);
        String url = miUrl;
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setMessage("Conectando . . .");
        progress.setCancelable(false);
        progress.show();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        progress.dismiss();
                        wVExplorador.loadDataWithBaseURL(null, response, "text/html", "UTF-8", null);
                        contenidoWeb = new StringBuilder(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                progress.dismiss();
                Toast.makeText(ExploradorWeb_Activity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });

        queue.add(stringRequest);
    }
    private void conectarAAHC(String url) {
        String texto = url;
        final ProgressDialog progress = new ProgressDialog(ExploradorWeb_Activity.this);


        AsyncHttpClient client = new AsyncHttpClient();
        client.get(texto, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                // cuando se recibe el estado de HTTP "4XX"
                progress.dismiss();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                progress.dismiss();
                wVExplorador.loadDataWithBaseURL(null, responseString, "text/html", "UTF-8", null);
                contenidoWeb = new StringBuilder(responseString);


            }

            @Override
            public void onStart() {

                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }
        });
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public void descargarPorAAHC(String url) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {

                    private ProgressDialog progreso;

                    @Override
                    public void onStart() {
                        // called before request is started
                        progreso = new ProgressDialog(ExploradorWeb_Activity.this);
                        progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                        progreso.setMessage("Conectando . . .");
                        progreso.show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String response) {
                        // called when response HTTP status is "200 OK"
                        progreso.dismiss();
                        fin = System.currentTimeMillis();
                        wVExplorador.loadData(response, "text/html", "UTF-8");
                        txvDuracion.setText("Duracion: " + String.valueOf(fin - inicio)
                                + " milisegundos");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                        // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                        progreso.dismiss();
                        Toast.makeText(ExploradorWeb_Activity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }


    class ConectionAsynt extends AsyncTask<String, Integer, Result> {


        private ProgressDialog progreso;
        private Context context;
        private String encoding;

        public ConectionAsynt(Context context, String encoding) {

            this.context = context;
            this.encoding = encoding;
        }

        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(true);
            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    ConectionAsynt.this.cancel(true);
                }
            });
            progreso.show();
        }

        @Override
        protected Result doInBackground(String... params) {
            int i = 1;
            Result result = new Result();
            publishProgress(i);
            result = conectJava(params[0]);
            return result;
        }

        private String read(InputStream entrada) {

            BufferedReader in;
            String linea;
            StringBuilder miCadena = new StringBuilder();
            in = new BufferedReader(new InputStreamReader(entrada), 32000);
            try {
                while ((linea = in.readLine()) != null)
                    miCadena.append(linea);
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return miCadena.toString();
        }

        private Result conectJava(String texto) {

            URL url;
            HttpURLConnection urlConnection = null;
            int respuesta;
            Result resultado = new Result();
            try {
                url = new URL(texto);
                urlConnection = (HttpURLConnection) url.openConnection();
                respuesta = urlConnection.getResponseCode();
                if (respuesta == HttpURLConnection.HTTP_OK) {
                    resultado.setCode(true);
                    resultado.setContent(read(urlConnection.getInputStream()));


                } else {
                    resultado.setCode(false);
                    resultado.setMenssage("Error en el acceso a la web: " + String.valueOf(respuesta));
                }
            } catch (IOException e) {
                resultado.setCode(false);
                resultado.setMenssage("Excepción: " + e.getMessage());
            } finally {
                try {
                    if (urlConnection != null)
                        urlConnection.disconnect();
                } catch (Exception e) {
                    resultado.setCode(false);
                    resultado.setMenssage("Excepción: " + e.getMessage());
                }

            }

            return resultado;
        }

        @Override
        protected void onPostExecute(Result result) {
            progreso.dismiss();

            if (result.isCode()) {

                wVExplorador.loadDataWithBaseURL(null, result.getContent(), "text/html", encoding, null);
                contenidoWeb = new StringBuilder(result.getContent());
            }

        }

        protected void onProgressUpdate(Integer... progress) {
            progreso.setMessage("Conectando " + Integer.toString(progress[0]));
        }
    }
}
