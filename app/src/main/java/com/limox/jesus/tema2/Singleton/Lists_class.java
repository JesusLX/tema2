package com.limox.jesus.tema2.Singleton;

import android.content.Context;

import com.limox.jesus.tema2.model.Contacto;
import com.limox.jesus.tema2.utilities.Ficheros;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jesus on 6/11/16.
 */

public class Lists_class {
    List<Contacto> contactos;
    String fichero;
    Ficheros cfc;
    public final static String ficheroContactos = "contactos.lx";
    static Lists_class me;

    public static Lists_class get(){
        if (me == null){
            me = new Lists_class();
        }
        return me;
    }

    private Lists_class() {
        contactos = new ArrayList<>();
    }
    public  List<Contacto> getContactos(String fichero, Context context){
        Ficheros lectorFicheros  = new Ficheros();
        List<String> strContactos = lectorFicheros.leerListaInterna(fichero,context);
        contactos = new ArrayList<>();
        StringBuilder name = new StringBuilder();
        StringBuilder numbr = new StringBuilder();
        StringBuilder  email = new StringBuilder();
        for (String c: strContactos) {
            String[] contenido = c.split(";");
            try {
                name.append(contenido[0]);
            }catch (Exception e){}
            try {
                numbr.append(contenido[1]);
            }catch (Exception e){}
            try {
                email.append(contenido[2]);
            }catch (Exception e){}

            contactos.add(new Contacto(name.toString(),numbr.toString(),email.toString()));
            name = new StringBuilder();
            numbr = new StringBuilder();
            email = new StringBuilder();
        }
        return contactos;
    }
    public void addContacts(String fichero, Context context, String nombre, String numero, String email){
        Ficheros escritoFichero = new Ficheros();

        contactos.add(new Contacto(nombre,numero,email));
        escritoFichero.escribirInterna(context,fichero, deContactosAString());
    }
    private List<String> deContactosAString(){
        ArrayList<String> srtContactos = new ArrayList<>();
        for (Contacto contacto: contactos ) {
            srtContactos.add(contacto.getNombre()+";"+contacto.getNumero()+";"+contacto.getEmail());
        }

        return srtContactos;
    }
}
