Tema 2
----------
 Ejercicios de ficheros (Locales y en Red)
=====================

Esta aplicación consta de varias *Activities*, empezando por una principal con *7 botones*, cada uno lleva a otra *Activity* diferente.
Las siguientes *Activities* contienen:
1. **Agenda.** 
2. **Alarmas.**
3. **MenstruApp.**
4. **Explorador web.**
5. **Visor de imágenes.**
6. **Conversor de monedas.**
7. **Subir ficheros.**

----------
Activity Principal
------------------------


Aquí se presenta la primera Activity con los siete botones nombrada anteriormente, los botones tienen los colores predefinidos por mi para aspecto único de la aplicación.

![1.png](https://bitbucket.org/repo/x4rxa4/images/4069244779-1.png)

1. Agenda
-------------
 ----------------------------------
![2.png](https://bitbucket.org/repo/x4rxa4/images/1829951262-2.png)

Aqui se ve una lista de los contactos creados, al darle al botón crear contacto aparece la siguiente activity:

![3.png](https://bitbucket.org/repo/x4rxa4/images/3940586103-3.png)

Aqui se rellenan los datos del contacto y si se confirma se guarda en un archivo de la memoria interna del movil para mostrarse en pantalla

2. Alarmas
----------------------------------------
----------------------
![4.png](https://bitbucket.org/repo/x4rxa4/images/69408559-4.png)

Aqui se ven las alarmas, la primera vez que se abre este layout se crea un fichero en memoria externa con las alarmas y después sólo hace falta leerlo.
Al darle al botón de comenzar empieza una cuenta atrás consecutiva de las alarmas, una detrás de otra y avisa de ello con un toast y una alarma sonora

![5.png](https://bitbucket.org/repo/x4rxa4/images/410587127-5.png)

3. MenstruApp
----------------------------------------
---------------------------
![6.png](https://bitbucket.org/repo/x4rxa4/images/1726939571-6.png)

Esta es la pantalla principal, el mensaje que aparece depende del momento de la app, si se ha entrado por primera vez sin que se haya configurado el fichero te lo dice o si hay un error al acceder a la tarjeta sd o en los casos de que se haya configurado dice si se está en un día fértil o no.
Al dar a configurar se puede poner para que te lo calcule

![7.png](https://bitbucket.org/repo/x4rxa4/images/1405840666-7.png)

![8.png](https://bitbucket.org/repo/x4rxa4/images/3712040486-8.png)

4. Explorador web 
-------------------------------
---------------------
![9.png](https://bitbucket.org/repo/x4rxa4/images/1598589813-9.png)

En la parte superior se introduce la url de la web que se quiere mostrar, debajo se muestra un conjunto de radio buttons para elegir la forma en la que se va a conseguir la conexión a la web

![10.png](https://bitbucket.org/repo/x4rxa4/images/3407659581-10.png)

Debajo puedes descargar la pagina web en un fichero con el nombre que le pongas.

5. Visor de imágenes  
-------------------------------
------------
![14.png](https://bitbucket.org/repo/x4rxa4/images/1143874973-14.png)

Esto es un visor de imagenes que se descargan por internet, en el campo de arriba introduces la url hacia el fichero contenedor de urls de imagenes y abajo puedes verlas de una en una pudiendo ir a la siguiente y la anterior

![15.png](https://bitbucket.org/repo/x4rxa4/images/714987642-15.png)

6. Conversor de monedas  
-------------------------------
------------
![11.png](https://bitbucket.org/repo/x4rxa4/images/458573916-11.png)

Es la version actualizada de la del ejercicio anterior, en este caso en vez de introducirle el valor del cambio directamente lo coge el fichero en internet que se le introduzca en el campo de abajo

7. Subir ficheros  
-------------------------------
------------
![12.png](https://bitbucket.org/repo/x4rxa4/images/1440569030-12.png)

Se busca un archivo dentro del movil usando un explorador de archivos al darle al botón de buscar archivos 

![13.png](https://bitbucket.org/repo/x4rxa4/images/3189617095-13.png)

el archivo seleccionado se guarda y al darle a Enviar lo mandas al servidor que hayas puesto en el campo de servidor